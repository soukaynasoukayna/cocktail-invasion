// ----------------------
// - FUNCTIONS MONSTERS -
// ----------------------

// Fonction qui permet aux tours d'attaquer les monstres 
function monsterHitByTower(Tower,monsters,Player) {

	// SI le monstre est toujours à distance :
	if ( (Tower.minTop < Tower.monsterTarget.top) && (Tower.monsterTarget.top < Tower.maxTop) && (Tower.minLeft < Tower.monsterTarget.left) && (Tower.monsterTarget.left < Tower.maxLeft) && Tower.monsterTarget.hp > 0) {
		// On retire des HP au monstre cible
		Tower.monsterTarget.hp -= 1*Tower.damage;
		Tower.monsterTarget.monsterLosingHP(monsters,Player);
	}
	// SINON, on retire la target de la tour
	else {
		Tower.monsterTarget = null;
	}	
}

// Fonction qui définit pour chaque tour le monstre le plus proche
function monsterClosetToTheTower(Tower, monsters){
	var hypo,
		distX   = 0,
		distY   = 0,
		distMin = 10000;
		
	// Pour chaque monstre
	for (var i = 0, c = monsters.length; i < c; i++) {

		// SI la tour peut attaquer (elle a fini d'être construite) ET que le montre est à distance de tir
		if ( (Tower.canAttack == true) && (Tower.minTop < monsters[i].top) && (monsters[i].top < Tower.maxTop) && (Tower.minLeft < monsters[i].left) && (monsters[i].left < Tower.maxLeft) ) {
			distX = Math.abs(monsters[i].left - Tower.left);
			distY = Math.abs(monsters[i].top - Tower.top);
			hypo  = calcHypotenuse(distX, distY); // On calcule la distance entre le monstre et la tour

			// Si la distance est inférieur on définit la nouvelle cible
			if (hypo < distMin) {
				distMin = hypo;
				Tower.monsterTarget = monsters[i];
			}
		}
	}

	if (Tower.monsterTarget != null) {
		Tower.enableShockwave();
		// SI le monstre possède déjà l'effet de la tour, on ajoute 1
		if (Tower.monsterTarget.state[Tower.type]) {
			Tower.monsterTarget.state[Tower.type] += 1;
		}
		// SINON c'est la première fois que la tour subit l'effet
		else {
			Tower.monsterTarget.state[Tower.type] = 1;
		}

		if ((Tower.type == "Fire") || (Tower.type == "Ice")) {
			Tower.monsterTarget.timerEffect(Tower);	
		}
	} else {
		Tower.disableShockwave();
	}
}

// Fonction qui déplace les monstres et permet aux tours d'attaquer
function monsterMove(Player, Parcours, monsters, towers, speed) {
	var monsterMove = setInterval(function(){

		course(Parcours, monsters, Player);

		// On lance les vérifications pour attaquer ou non les monstres
		for (var i = 0; i < towers.length; i++) {

			// Si la tour a une cible :
			if (towers[i].monsterTarget !== null) {
				
				// La tour attaque le monstre le plus proche
				monsterHitByTower(towers[i],monsters,Player);
			}
			// Sinon, elle recherche la cible la plus proche
			else {
				monsterClosetToTheTower(towers[i],monsters)	
			}			
		}

		// Si il n'y a plus de monstres, on arrête le jeu et on passe à la vague suivante
		if (monsters.length == 0) {
			clearInterval(monsterMove);

			// On augmente le niveau du joueur de 1
			Player.level++;
			$('.infos span.level').fadeOut('slow', function() {
				$(this).text(Player.level);
			}).fadeIn();

			// SI le joueur a encore des vies, on relance une vague
			if (Player.life > 0) {
				// On appelle la fonction qui relance une vague
				startGame(Player, Parcours, monsters, towers);
			}
		}
	}, speed);
}

// Fonction qui crée un monstre
function Monster (top,left,hp,name,money,img,speed) {
	this.top     = top;
	this.topTemp = top;
	this.left    = left;
	this.leftTemp= 0;
	this.hp      = hp;
	this.name    = name;
	this.money   = money;
	this.img     = img;
	this.hpMax   = hp;
	this.cStep   = 0;
	this.speed   = speed;
	this.speedMax= speed;
	this.state   = {};

	this.create = function() {
		var html  = $('<div class="monster" style="top:' + this.top + 'px; left: ' + this.left + 'px;" data-hp="' + this.hp + '" data-name="' + this.name + '">' +
						'<img src="' + this.img + '" alt="Monstre ' + this.name + '">' +
						'<div class="progress-bar bg-success" role="progressbar" aria-valuemin="0" aria-valuemax="' + hp + '" aria-valuenow="' + this.hp + '" style="width:100%;">' + hp + '</div>' +
					'</div>');

		this.DOM = html;
		$('.monsters').append(html);
	};

	// On appelle la méthode qui crée un monstre (html)
	this.create();

	// Méthode qui permet de déplacer le monstre vers le haut/bas
	this.moveUpDown = function () {
		$(this.DOM).css('top', this.top+'px');
	};

	// Méthode qui permet de déplacer le monstre vers la droite/gauche
	this.moveLeftRight = function () {
		$(this.DOM).css('left', this.left+'px');
	};

	this.monsterLosingHP = function(monsters,Player) {
		// On change l'affichage de la barre de HP du monstre
		$(this.DOM).find('div.progress-bar').text(Math.max(parseInt(this.hp), 0));
		$(this.DOM).find('div.progress-bar').css('width',hpPourcent(this.hp, this.hpMax) + '%');
		$(this.DOM).find('div.progress-bar').attr('aria-valuenow',this.hp);

		// Si le monstre n'a plus de hp
		if (this.hp <= 0){

			// On supprime le monstre du jeu (html)
			$(this.DOM).fadeOut('slow',function(){
				$(this).remove();
			});

			// On supprime le montre du tableau des monstres
			for (var i = 0; i < monsters.length; i++) {
			    if (monsters[i] == this) {
			        monsters.splice(i,1);
			    }
			}

			// On fait gagner de l'argent au joueur
			Player.money += this.money;
			//console.log(Player.money);
			$('.infos span.money').text(Player.money);

			// On retire la cible de la tour
			//Tower.monsterTarget = null;

			// On réactualise l'affichage des tours à créer
			displayTowers(Player);
		}
	}

	this.underTheEffect = function(monsters,Player) {
		if (this.state['Ice'] > 0) {
			$(this.DOM).addClass('frozen');
			this.speed = 0.5 / this.state['Ice']; 
		}
		else {
			$(this.DOM).removeClass('frozen');
			this.speed = this.speedMax;
		}

		if (this.state['Fire'] > 0) {
			$(this.DOM).addClass('burning');
			this.hp -= 0.1 * this.state['Fire']; // 0.1 each px and 1px each 5ms => 1sec shot = (1000/5) = 200px / sec == each second = 20hp
			this.monsterLosingHP(monsters,Player);
		}
		else {
			$(this.DOM).removeClass('burning');
		}
	}

	this.timerEffect = function(Tower) {
		var count = Tower.powerTime;
		var that  = this;

		var timer = setInterval(function() {
			count--;
			
			if ((count == 0) || (that.hp <= 0)) {
				//console.log('Fin de l\'effet : ' + Tower.type);
				clearInterval(timer);
				that.state[Tower.type]--;
			}
			//console.log(Tower.type + ' : ' + count);
		},1000);
	}
}